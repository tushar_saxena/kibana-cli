#!/usr/bin/env python
# -*- coding: utf-8 -*-

# -----------------------------------------------------------------------------
# Title       : logger.py
# Author      : Tushar Saxena (tushar.saxena@gmail.com)
# Date        : 17-Sep-2014
# Description : Kibana Command Line Interface - Logging Helper
# -----------------------------------------------------------------------------

import datetime
import json
import logging
import os
import uuid
import time

from collections import OrderedDict

DEFAULT_LOG_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'logs')
LOG_SUFFIX = 'log'

# -------------------------------------
class Logger():
	# -------------------------------------

	# -------------------------------------
	def __init__(self, name=None, debug=False, verbose=False, log_dir=DEFAULT_LOG_DIR):
		# -------------------------------------
		# Init
		self.name = name
		self.debug = debug
		self.verbose = verbose
		self.log_dir = log_dir

		self.uuid = str(uuid.uuid4())
		self.message_fields = ['timestamp', 'uuid', 'module', 'operation', 'level', 'msg']

		# Map named logging levels onto self
		self.DEBUG = logging.DEBUG
		self.INFO = logging.INFO
		self.WARNING = logging.WARNING
		self.ERROR = logging.ERROR
		self.CRITICAL = logging.CRITICAL

		# Check if log_dir exists
		if not os.path.exists(self.log_dir):
			os.makedirs(self.log_dir)

		# Create a new file logging instance
		self.file_logger = logging.getLogger(self.name + '_file')
		self.file_logger.setLevel(self.DEBUG)

		timestamp = time.strftime('%Y%m%d.%H%M%S')
		log_filename = '%s/%s.%s.%s' % (self.log_dir, self.name, timestamp, LOG_SUFFIX)
		file_loghandler = logging.FileHandler(log_filename)
		file_loghandler.setLevel(self.DEBUG)
		self.file_logger.addHandler(file_loghandler)

		# Create a new stdout logging instance
		self.stdout_logger = logging.getLogger(self.name + '_stdout')
		self.stdout_logger.setLevel(self.DEBUG)

		stdout_loghandler = logging.StreamHandler()
		stdout_loghandler.setLevel(logging.DEBUG)

		self.stdout_logger.addHandler(stdout_loghandler)

	# -------------------------------------
	def log(self, level=logging.INFO, verbose=False, message=None):
		# -------------------------------------
		log_to_file, log_to_stdout = self.check_log_channel(verbose=verbose, level=level)

		if self.file_logger and log_to_file:
			self.file_logger.log(level, message)
		if self.stdout_logger and log_to_stdout:
			self.stdout_logger.log(level, message)

	# -------------------------------------
	def logf(self, level=logging.INFO, verbose=False, *args, **kwargs):
		# -------------------------------------
		message_dict = dict()
		message_dict['level'] = level
		message_dict['uuid'] = self.uuid
		message_dict['timestamp'] = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S.%f')

		for k, v in kwargs.iteritems():
			message_dict[k] = v

		formatted_log_message_file = self.formatLogEntry(message_dict)
		log_to_file, log_to_stdout = self.check_log_channel(verbose=verbose, level=level)

		if self.file_logger and log_to_file:
			self.file_logger.log(level, formatted_log_message_file)
		if self.stdout_logger and log_to_stdout:
			self.stdout_logger.log(level, message_dict['msg'])

	# -------------------------------------
	def check_log_channel(self, verbose=False, level=logging.INFO):
		# -------------------------------------
		log_to_stdout = False
		log_to_file = False

		if verbose or self.verbose:
			if self.debug:
				if level >= self.DEBUG:
					log_to_stdout = True
				if level >= self.DEBUG:
					log_to_file = True
			else:
				if level >= self.INFO:
					log_to_stdout = True
				if level >= self.INFO:
					log_to_file = True
		else:
			if self.debug:
				if level > self.INFO:
					log_to_stdout = True
				if level >= self.DEBUG:
					log_to_file = True
			else:
				if level > self.INFO:
					log_to_stdout = True
				if level >= self.INFO:
					log_to_file = True

		return log_to_file, log_to_stdout

	# -------------------------------------
	def buildLogEntry(self, *args, **kwargs):
		# -------------------------------------
		log_entry = dict()

		for k, v in kwargs.iteritems():
			log_entry[k] = v

		return self.formatLogEntry(log_entry)

	# -------------------------------------
	def formatLogEntry(self, message_dict):
		# -------------------------------------
		formatted_message = OrderedDict()

		for field in self.message_fields:
			if field in message_dict:
				formatted_message[field] = message_dict[field]
			else:
				formatted_message[field] = None

		for field in sorted(message_dict.keys()):
			if field not in formatted_message.keys():
				formatted_message[field] = message_dict[field]

		formatted_message['level'] = logging.getLevelName(formatted_message['level'])

		return json.dumps(formatted_message)

# -------------------------------------
if __name__ == '__main__':
	# -------------------------------------
	print 'Error : This python script cannot be run as a standalone program.'
