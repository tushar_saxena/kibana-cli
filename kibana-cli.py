#!/usr/bin/env python
# -*- coding: utf-8 -*-

# -----------------------------------------------------------------------------
# Title         : kibana-cli.py
# Author        : Tushar Saxena (tushar.saxena@gmail.com)
# Date          : 27-Nov-2014
# Description   : Kibana Command Line Interface
# -----------------------------------------------------------------------------

# -------------------------------------
# Imports
# -------------------------------------
import json
import os
import sys
import traceback
import uuid
import time

from logger import Logger
from optparse import OptionParser
from requests import session, codes

# -------------------------------------
# Globals
# -------------------------------------
APP_NAME = 'kibana-cli'
UUID = uuid.uuid4()
DEFAULT_QUERY = 'Hello, World!'
DEFAULT_NUM_RESULTS = 10
DEFAULT_CONFIG_FILE = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'config', 'config.json')

# -------------------------------------
def parse_options():
	# -------------------------------------

	usage = 'Usage: %prog [options]'
	parser = OptionParser(usage)

	parser.add_option('-q', '--query', action='store', dest='query', default=DEFAULT_QUERY, help='Query term [default=%default]')
	parser.add_option('-n', '--numresults', action='store', dest='num_results', default=DEFAULT_NUM_RESULTS, help='Number of results [default=%default]')
	parser.add_option('-c', '--config', action='store', dest='config', default=DEFAULT_CONFIG_FILE, help='Configuration file to use [default=%default]')
	parser.add_option('-v', '--verbose', action='store_true', dest='verbose', default=False, help='Show verbose output [default=%default]')
	parser.add_option('-d', '--debug', action='store_true', dest='debug', default=False, help='Show debugging output [default=%default]')

	(options, args) = parser.parse_args()

	if len(args):
		parser.print_help()
		sys.exit(1)

	return options

# -------------------------------------
def format_output(raw_json_data, execution_time):
	# -------------------------------------

	print '-' * 30, 'RESULTS', '-' * 30
	json_data = json.loads(raw_json_data)
	for hit in json_data['hits']['hits']:
		print json.dumps(hit, indent=4)

	print '-' * 30, ' STATS ', '-' * 30
	print 'Displaying %s of %s results' % (options.num_results, json_data['hits']['total'])
	print 'Elasticsearch query time :', float(json_data['took']) / 1000
	print 'Overall       query time :', round(execution_time, 3)

# -------------------------------------
if __name__ == '__main__':
	# -------------------------------------
	# Parse command line args
	options = parse_options()

	# Initialize logger
	logger = Logger(name=APP_NAME, debug=options.debug, verbose=options.verbose)

	# Load config file
	if os.path.exists(options.config):
		# Load config file
		with open(options.config) as json_data_file:
			config = json.load(json_data_file)
		log_msg = 'Loaded config file <%s>' % (options.config)
		logger.logf(level=logger.INFO, module=APP_NAME, operation='config', msg=log_msg)
	else:
		log_msg = 'Config file <%s> does not exist. Exiting ...' % (options.config)
		logger.logf(level=logger.INFO, module=APP_NAME, operation='config', msg=log_msg)
		sys.exit(1)

	# Execute command
	try:
		with session() as web_session:
			url = '%s:%s/_search?&size=%s&q="%s"' % (config['logstash']['host'], config['logstash']['port'], options.num_results, options.query)
			log_msg = 'Executing query, please stand by ...'
			logger.logf(level=logger.INFO, module=APP_NAME, operation='execute', msg=log_msg, verbose=True)
			t_start = time.time()
			response = web_session.get(url)
			execution_time = time.time() - t_start
			if response.status_code == codes.ok:
				format_output(response.text, execution_time)
			else:
				log_msg = 'Error executing query: %s | %s' % (response.status_code, response.reason)
				logger.logf(level=logger.ERROR, module=APP_NAME, operation='execute', msg=log_msg)
	except Exception as _:
		exc_type, exc_value, exc_traceback = sys.exc_info()
		log_msg = 'Exception = %s :: Stacktrace = %s' % (traceback.format_exception_only(exc_type, exc_value)[0][:-1], traceback.extract_tb(exc_traceback))
		logger.logf(level=logger.ERROR, module=APP_NAME, operation='execute', msg=log_msg)

	# Exit
	sys.exit(0)
