# *Kibana-CLI README*

## Introduction

Kibana-CLI is a command line interface which allows you to query Elasticsearch from the command line. It currently supports a very basic feature set, but more will be added over time, so stay tuned!

## Configuring Kibana-CLI

Take a look at the default configuration file provided in the config folder. It's fairly self-explanatory, you really only need to update the "host" field, the "port" defaults to 9200 for Elasticsearch, so you probobaly won't need to change it. Please note that the configuration file is written in strict JSON notation, so use a JSON validator like [JSONLint](http://jsonlint.com) to validate it.

## Executing Kibana-CLI

    $ python kibana-cli.py --help

    Usage: kibana-cli.py [options]
    Options:
      -h, --help            show this help message and exit
      -q QUERY, --query=QUERY
                            Query term [default=Hello, World!]
      -n NUM_RESULTS, --numresults=NUM_RESULTS
                            Number of results [default=10]
      -c CONFIG, --config=CONFIG
                            Configuration file to use
                            [default=/home/add1kted2ka0s/host/GIT/personal/kibana-
                            cli/config/config.json]
      -v, --verbose         Show verbose output [default=False]
      -d, --debug           Show debugging output [default=False]

Sample usage:
    
    $ python kibana-cli.py -n 50 -c config/my_config_file.json -q my_search_term

This will search the Elasticsearch indices for *my_search_term* and return the first *50* results using the *config/my_config_file.json* configuration file.